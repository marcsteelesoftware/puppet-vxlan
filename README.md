
# VXLAN

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with VXLAN](#setup)
    * [Setup requirements](#setup-requirements)
    * [Beginning with VXLAN](#beginning-with-VXLAN)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Configures VXLAN interfaces on Linux systems.

Creates and configured VXLAN interfaces on endpoints. Multicast is assumed and required for this module to operate.

## Setup

### Setup Requirements

Requires thornio/ip to obtain the IP address data type.

### Beginning with VXLAN

The very basic steps needed for a user to get the module up and running. This can include setup steps, if necessary, or it can be an example of the most basic use of the module.

## Usage

To enable VXLAN as a feature, use the following configuration:

class { 'vxlan'
    interfaces => {
        'tun0' => {
            'name' => 'tun0'
            'id' => 10,
            'group' => '239.0.0.1',
            'ttl' => 255,
            'physical' => 'ens192'
        }
    }
}

## Reference

Users need a complete list of your module's classes, types, defined types providers, facts, and functions, along with the parameters for each. You can provide this list either via Puppet Strings code comments or as a complete list in the README Reference section.

* If you are using Puppet Strings code comments, this Reference section should include Strings information so that your users know how to access your documentation.

* If you are not using Puppet Strings, include a list of all of your classes, defined types, and so on, along with their parameters. Each element in this listing should include:

  * The data type, if applicable.
  * A description of what the element does.
  * Valid values, if the data type doesn't make it obvious.
  * Default value, if any.

## Limitations

This is where you list OS compatibility, version compatibility, etc. If there are Known Issues, you might want to include them under their own heading here.

## Development

This was originally a part-time effort to get an elasticsearch cluster to work with a virtualised network.