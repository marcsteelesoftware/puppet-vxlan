# vxlan::interface
#
# Defines a specific VXLAN interface.
#
# @summary Defines a specific VXLAN interface.
#
# @param name
#   The name the interface will take on. E.g. tun0 or vxlan100
#
# @param id
#   The virtual network identifier. This is a unique 24 bit number for your network.
#
# @param group
#   The multicast IP address that will be used to communicate between VTEPs.
#
# @param ttl
#   The TTL on any VXLAN packet transmitted.
#
# @param physical
#   The physical (or bridge, lag, etc.) interface that the VXLAN interface will be attached to.
#

define vxlan::interface(
  String                  $interface  = undef,
  Integer                 $id         = undef,
  String                  $group      = undef,
  Integer                 $ttl        = 255,
  String                  $physical   = undef,
  Integer                 $port       = 4789,
  String                  $address    = undef,
) {

  # Sanity check the kernel version

  if versioncmp($facts['kernelmajversion'], '3.7') < 0 {
    fail("$facts['kernelmajversion'] is not a modern enough kernel version to support VXLAN.")
  }

  # Check the physical interface we want to tie to is real

  if (!($physical in $facts['interfaces'])) {
    fail("Interface $physical must exist for us to create the VXLAN interface on it.")
  }

  # Other sanity checks

  if ($ttl < 0 or $ttl > 255) {
    fail("The TTL value must be between 0 and 255 if you are overriding it.")
  }

  if (!$group) {
    fail("You must have a valid multicast address for the group.")
  }

  if (!$address) {
    fail("You must set and IP address and subnet for this node.")
  }

  # Check if the interface doesn't already exist

  if (!($interface in $facts['interfaces'])) {

    # Create the new interface

    exec { "ip link add $interface type vxlan id $id group $group dstport $port dev $physical ttl $ttl":
      path    => ['/usr/sbin/', '/bin/']
    }

    exec { "ip addr add $address dev $interface":
      path    => ['/usr/sbin/', '/bin/']
    }

    exec { "ip link set dev $interface up":
      path    => ['/usr/sbin/', '/bin/']
    }

  }

}
