# VXLAN::vxlan
#
# Enables VXLAN on Linux systems.
#
# @summary Enables VLAN interfaces.

class vxlan (
  Hash              $interfaces,
) {

  create_resources('vxlan::interface', $::vxlan::interfaces)

}
